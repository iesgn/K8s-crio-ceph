# Kubernetes: backend cri-o y almacenamiento en Ceph.

1. [Introducción](#introducción)

    1.1 [¿Qué es Kubernetes?](#qúe-es-kubernetes)

    1.2 [¿Qué es Cri-o? y ¿Por qué?](#qué-es-cri-o-y-por-qué)

    1.3 [¿Qué es Ceph?](#qué-es-ceph)

2. [Escenario](#escenario)

3. [Instalación](#instalación)

    4.1 [Cri-o](#cri-o)

    4.2 [Kubernetes](#kubernetes)

    4.3 [Ceph](#ceph)
    
    4.4 [Rbd-provisioner](#despliegue-de-rbd-provisioner)

4. [Funcionamiento](#funcionamiento)

5. [Conclusión](#conclusión)

6. [Trabajo por hacer](#trabajo-por-hacer)

7. [Fuentes](#fuentes)

---

## Introducción

El objetivo de este proyecto es llegar a conocer el funcionamiento de tres tecnologías nuevas como son: Kubernetes, Ceph y Cri-o. Estas tecnologías las voy a combinar y demostrar el 
potencial que tiene la mezcla de estas tres tecnologías. Los roles que van a tener cada una de las tecnologías es la siguiente:

* Ceph: Clúster de almacenamiento nos va a proveer los volúmenes rbd
* Cri-o: Backend de Kubernetes encargado de generar contenedores
* Kubernetes: Orquestar los anteriores elementos para poner en producción aplicaciones. 

Vamos ver mas detenidamente cada uno de los elementos que he tocado en el proyecto.

### ¿Qúe es Kubernetes?

Kubernetes se trata de un orquestador de contenedores de aplicación escrito en Go y con licencia Apache-2.0, fue lanzado el 21 de julio de 2015 por Google que rápidamente se alió
con la Linux Foundation para crear la Cloud Native Computing Foundation (CNCF) y dejó el proyecto en sus manos. Este lanzamiento eclipsó a la competencia, tanto Docker Swarm como 
Apache Mesos, creando una fiebre hacia Kubernetes fuera de toda duda y dejándolo como el orquestador de contenedores más expandido hasta la fecha.

Aquí una gráfica comparando los principales orquestadores de contenedores: Kubernetes(Azul), Mesos(Rojo) y Swarm(Amarillo). También he añadido OpenShift(verde) que siendo su backend Kubernetes es una alternativa a las otras tecnologías.

![Leyenda](/images/description.png)

![Grafica Comparativa](/images/graph.png)

Ahora, Kubernetes está siendo utilizado por diversas compañías como base de otras tecnologías que están desarrollando, la más potente y conocida es
Openshift la solución de RedHat.

Algunos conceptos que tenemos que tener en cuenta son los siguientes:

* Deployment: Se declaran los contenedores que queremos desplegar y las características que van a tener.
* ReplicaSet: Mantiene el número de pods que le hemos pedido.
* Pod: Es la combinación de contenedor o contenedores, espacio de red y almacenamiento, es la únidad básica en Kubernetes.
* Service: La manera que tenemos de exponer un deployment.
* Volume: Pensado para los StatefulSet para mantener la información de manera persistente.
* Namespace: Para el aislamiento de Pods.
* Label: El marcado de recursos del clúster es fundamental para poder hacer acciones en lote con una o varias label declaradas.
* Jobs: Es un pod que realiza un proceso determinado y cuando lo ejecuta guarda el resultado hasta que se elimina.

En Kubernetes podemos diferenciar dos tipos de componentes:

* "Master Components" o componentes que solo los encontramos en los nodos que hacen de master.
* "Node Components" estos los que nos encontramos en todos los nodos de Kubernetes.

**MASTER COMPONENTS:**

*Kube-apiserver:*  

Se encarga de exponer la API de Kubernetes y ejecutar/validar las órdenes en el clúster.

*Etcd:*  

En una base de datos clave/valor que almacena toda la información del clúster, es extremadamente rápida y es capaz de estar distribuida y sincronizada.

*Kube-Scheduler:*  

Su propósito es elegir a que nodo va a ir cada pod dependiendo de las condiciones que le pongamos en el deployment: affinitys, antiaffinitys, workloads, data locality...

*Kube-controller-manager:*  

Se encarga de ejecutar todos los controladores:
* Node controller: Vigila que todos los nodos estén en estado "ready".
* Replication controller: Mantiene el número solicitado de pods en el clúster.
* Endpoints controller: Propaga los endpoints.
* Service Accounts y Token Controllers: Permisos y tokens.

*Cloud-controller-manager:*  

En caso que despleguemos nuestro clúster de Kubernetes en un entorno cloud como AWS o GKE este componente se va a encargar de interactuar con la API del entorno.

**NODE COMONENTS:**

*Kubelet:*

Se encuentra en todos los nodos de nuestra infraestructura, es el primario, trabaja en términos de PodSpec (yaml que descirbe un pod), y se encarga de controlar los contenedores creados por Kubernetes y comparar el estado deseado con el estado actual. A la hora del despliegue es el que enlaza las máquinas.

*Kube-proxy:*

Refleja los servicios definidos en la API de Kubernetes en cada nodo con con peticiones TCP/UDP simples o balanceadas a través de los diferentes backends. Las direcciónes del clúster IP y puerto d elos diferentes Docker-links están disponibles mediante variables de entorno abiertas por el servidor proxy.

*Container Runtime:*

Es el software que se encarga de crear los contendores. Kubernetes originalmente estaba pensado para contenedores Docker pero con el paso de tiempo han ido habilitando nuevos backends como RKT o en nuestro caso uno bastante reciente
y en estado prematuro como cri-o.

### ¿Qué es Cri-o? y ¿Por qué?

Cri-o es un software de creación de contenedores escrito go y desarrollado por Red-Hat dentro del proyecto Atomic. Red-Hat ha querido seguir la filosofía UNIX en este proyecto, ya que para intentar hacer competencia a docker,
ha creado diferentes softwares para emularlo. Por ejemplo tenemos cri-o como creador de contenedor, buildah para crear imágenes y no un software pesado y gigantesco que lo engloba todo.

Esto no es todo, cri-o además, es compatible con imágenes Docker por lo podremos reutilizar las ya desplegadas. Cri-o, además es capaz de realizar centralizados de logs y métricas, así como soportar diferentes sistemas de ficheros.

Y poco más hay que contar, se dedica a crear contendores, sin más.

El motivo de haber elegido cri-o como backend par Kubernetes es ese, al ser un software que solo se dedica a crear contenedores nuestro entorno requerirá menos recursos "en teoría"
para desplegar nuestro clúster de Kubernetes.

### ¿Qué es Ceph?

Es un sistema de almacenamiento distribuido y diseñado bajo licencia GPL como software libre, tiene parte de su código escrito en Python y otro en C++. Nos permite almacenamiento de objetos,
ofrecer dispositivos de bloques para almacenamiento y desplegar un sistema de ficheros.

La base de este sistema de almacenamiento es RADOS (Reliable Autonomic Distributed Object Store) y CRUSH (Controlled Replication Under Scalable Hashing), este último es el algoritmo que optimiza la utilización de los datos.
Nos proporciona también una interfaz REST que la expone Ceph Object Gateway (RGW) y los discos virtuales Ceph Block Device (RBD).

Ceph es una gran alternativa a sistemas de almacenamiento tipo SAN o NAS, pero ¿por qué elegí Ceph en lugar de soluciones como trident o nfs? Ceph nos proporcina las siguiente características:

* Rendimiento bastante alto
* Coste relativamente bajo
* Para crecer, solo es necesario añadir más equipos, elasticidad horizontal
* Muy flexible
* Control total al ser software libre
* Independencia de fabricantes

Ceph tiene diversos componentes que se comunican entre si para mantener la integridad del clúster de almacenamiento estos componentes son:

*OSD's:*

Es el demonio de almacenamiento de objetos, suele estar en el nodo que contiene los discos que son utilizados por el clúster, no se mezcla con el monitor. Además de maneja la replicación de los objetos y su recuperación. Balancea la carga de datos por el clúster y proporciona
datos del estado a los monitores y managers.

El mínimo son 3.

*Monitor:*

Se encarga de mantener un estado coherente en el clúster, además del mapeo de de los componentes en el esquema. Es esencial para la coordinación de los demonios.

Para entornos de producción serían necesarios un mínimo de 3.

*Servidor de metadatos:*

Almacena los metadatos de Ceph Filesystem, permite comandos básicos de POSIX filesystem y quita carga al clúster de almacenamiento ceph.

Mínimo 1.

*Managers:*

Incluido en la versión luminous se encarga de las métricas y del estado del clúster, así como de la utilización del espacio del clúster. Permite plugins de monitorización basados en pyton, incluye frontal web y REST API.

Necesarios mínimos 2 para alta disponibilidad.

## Escenario

El escenario se basa en dos nodos físicos:

* nodo1: 4 cores, 16GB de ram e hipervisor KVM
* nodo2: 8 cores, 8GB de ram e hipervisor KVM

En el nodo 1 se encuentra el clúster de Kubernetes repartido en tres máquina virtuales con Ubuntu 16.04 cada una de ellas con 2GB de RAM y 2 cores virtuales.

En el nodo2 está el clúster de almacemiento de Ceph en cuatro máquinas virtuales Debian Stretch, cada una de 1 GB de ram, 1 core virtual y un disco adicional de 20 GB.

Cada clúster está en una red diferentes por lo que he tenido que crear un router para enlazarlas. Mientra que Kubernetes está en la red doméstica 192.168.1.0/24, Ceph está en una red interna aislada en la 192.168.122.0/24.

El esquema es el siguiente:

![Esquema](/images/schema.png)

## Instalación

### Cri-o

Ahora voy a hacer la instalación de cri-o, esta instalación se ha realizado en cada uno de los nodos del clúster de Kubernetes para utilizarlo como backend.

Los binarios que voy a tener al final del proceso serán:

* crio - El software que maneja los pods
* crictl - El cliente CRI
* cni - La interfaz de red para los contendores.
* runc - El runtime con el estandar OCI para lanzar contendores.

#### runc

Descargo el binarios de Runc:

``` 
wget https://github.com/opencontainers/runc/releases/download/v1.0.0-rc4/runc.amd64
```

Le doy permisos de ejecución al binario:

```
chmod +x runc.amd64 && sudo mv runc.amd64 /usr/bin/runc
```

Para ver la correcta instalación veo la versión de Runc:

```
runc -version
```

#### cri-o

Para cri-o necesito instalarlo desde las fuentes, ya que no dispone de binario.

Instalo el runtime de Go:

```
wget https://storage.googleapis.com/golang/go1.8.5.linux-amd64.tar.gz
sudo tar -xvf go1.8.5.linux-amd64.tar.gz -C /usr/local/
mkdir -p $HOME/go/src
export GOPATH=$HOME/go && export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
```

Instalo crictl

```
go get github.com/kubernetes-incubator/cri-tools/cmd/crictl
cd $GOPATH/src/github.com/kubernetes-incubator/cri-tools
make
make install
```

Compilo desde las fuentes:
```
sudo apt-get update && apt-get install -y libglib2.0-dev \
                                          libseccomp-dev \
                                          libgpgme11-dev \
                                          libdevmapper-dev \
                                          make \
                                          git
```

```
go get -d github.com/kubernetes-incubator/cri-o
cd $GOPATH/src/github.com/kubernetes-incubator/cri-o
make install.tools
sudo make install.config
make
sudo make install
```
Una vez se haya instalado todo, voy a habilitar los registries en el fichero de configuración de cri-o ``` /etc/crio/crio.conf ``` para que pueda acceder a descargar imágenes docker.

```
registries = ['registry.access.redhat.com', 'registry.fedoraproject.org', 'docker.io']
```

Creo el demonio de cri-o:
 
 ```
sudo sh -c 'echo "[Unit]
Description=OCI-based implementation of Kubernetes Container Runtime Interface
Documentation=https://github.com/kubernetes-incubator/cri-o

[Service]
ExecStart=/usr/local/bin/crio --log-level debug
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/crio.service'
 ```

Lo activo:

```
sudo systemctl daemon-reload && systemctl enable crio && systemctl start crio
```

Creo el endpoint de crio:

```
sudo crictl --runtime-endpoint unix:///var/run/crio/crio.sock version
```

Para hacerlo permanente tengo que exportar la siguiente variable de entorno:

```
export $CRI_RUNTIME_ENDPOINT=unix:///var/run/crio/crio.sock version
```

#### CNI

Para descargar CNI:

```
go get -d github.com/containernetworking/plugins
cd $GOPATH/src/github.com/containernetworking/plugins
```

Hago build a CNI plugins:

```
./build.sh
```

Instalo los plugins:

```
sudo mkdir -p /opt/cni/bin
sudo cp bin/* /opt/cni/bin/
```

#### Configurar CNI

```
sudo mkdir -p /etc/cni/net.d
```

```
sudo sh -c 'cat >/etc/cni/net.d/10-mynet.conf <<-EOF
{
    "cniVersion": "0.2.0",
    "name": "mynet",
    "type": "bridge",
    "bridge": "cni0",
    "isGateway": true,
    "ipMasq": true,
    "ipam": {
        "type": "host-local",
        "subnet": "10.88.0.0/16",
        "routes": [
            { "dst": "0.0.0.0/0"  }
        ]
    }
}
EOF'
```

```
sudo sh -c 'cat >/etc/cni/net.d/99-loopback.conf <<-EOF
{
    "cniVersion": "0.2.0",
    "type": "loopback"
}
EOF'
```

E instalar skopeo-containers desde el repositorio de proyecto atomic:

```
sudo add-apt-repository ppa:projectatomic/ppa
sudo apt-get update
sudo apt-get install skopeo-containers -y
```

Y aplico los cambios a crio:

```
sudo systemctl restart crio
```

### Kubernetes

Durante la instalación de Kubernetes he encontrado una peculiaridad para tener crio como backend:

* ```Necesitamos tener docker instalado obligatoriamente, este "error" está corregido en la siguiente release de Kubernetes (1.11).```

Teniendo esto en cuenta voy a instalar el clúster de Kubernetes:

#### Instalar kubeadm, kubelet y kubectl

```
apt-get update && apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubelet kubeadm kubectl
```

Una vez instalados modifico el demonio de kubelet para que busque el socket de cri-o:

<pre>[Service]
Environment=&quot;KUBELET_KUBECONFIG_ARGS=--bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf --kubeconfig=/etc/kubernetes/kubelet.conf&quot;
Environment=&quot;KUBELET_SYSTEM_PODS_ARGS=--pod-manifest-path=/etc/kubernetes/manifests --allow-privileged=true --container-runtime=remote --container-runtime-endpoint=unix:///var/run/crio/crio.sock --runtime-request-timeout=10m &quot;
Environment=&quot;KUBELET_NETWORK_ARGS=--network-plugin=cni --cni-conf-dir=/etc/cni/net.d --cni-bin-dir=/opt/cni/bin&quot;
Environment=&quot;KUBELET_DNS_ARGS=--cluster-dns=10.96.0.10 --cluster-domain=cluster.local&quot;
Environment=&quot;KUBELET_AUTHZ_ARGS=--authorization-mode=Webhook --client-ca-file=/etc/kubernetes/pki/ca.crt&quot;
Environment=&quot;KUBELET_CADVISOR_ARGS=--cadvisor-port=0&quot;
Environment=&quot;KUBELET_CERTIFICATE_ARGS=--rotate-certificates=true --cert-dir=/var/lib/kubelet/pki&quot;
ExecStart=
ExecStart=/usr/bin/kubelet $KUBELET_KUBECONFIG_ARGS $KUBELET_SYSTEM_PODS_ARGS $KUBELET_NETWORK_ARGS $KUBELET_DNS_ARGS $KUBELET_AUTHZ_ARGS $KUBELET_CADVISOR_ARGS $KUBELET_CERTIFICATE_ARGS $KUBELET_EXTRA_ARGS
</pre>

También tengo que modificar el fichero de configuración de cni ```/etc/cni/net.d/10-mynet.conf```:

<pre>{
     &quot;name&quot;: &quot;mynet&quot;,
     &quot;type&quot;: &quot;flannel&quot;
}
</pre>

Una vez esté todo iniciado el servicio de kubelet, pero al ver su estado veo que se encuentra en estado de error, esto es por lo que he dicho antes:

<pre>
jun 09 10:58:22 k8s-node2 systemd[1]: kubelet.service: Service hold-off time over, scheduling restart.
jun 09 10:58:22 k8s-node2 systemd[1]: Stopped kubelet: The Kubernetes Node Agent.
jun 09 10:58:22 k8s-node2 systemd[1]: Started kubelet: The Kubernetes Node Agent.
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.001939    1982 feature_gate.go:226] feature gates: &amp;{{} map[]}
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.020298    1982 server.go:376] Version: v1.10.3
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.020330    1982 feature_gate.go:226] feature gates: &amp;{{} map[]}
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.020407    1982 plugins.go:89] No cloud provider specified.
jun 09 10:58:23 k8s-node2 kubelet[1982]: W0609 10:58:23.020427    1982 server.go:517] standalone mode, no API client
jun 09 10:58:23 k8s-node2 kubelet[1982]: W0609 10:58:23.022902    1982 server.go:433] No api server defined - no events will be sent to API server.
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.023128    1982 server.go:614] --cgroups-per-qos enabled, but --cgroup-root was not specified.  defaulting to /
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.023416    1982 container_manager_linux.go:242] container manager verified user specified cgroup-root exists: /
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.023533    1982 container_manager_linux.go:247] Creating Container Manager object based on Node Config: {RuntimeCgroupsName: SystemCgroupsName: KubeletCgroupsName: ContainerRuntime:docker CgroupsPerQOS:true CgroupRoot:/ CgroupDriver:cgroupfs KubeletRootDir:/var/lib/kubelet ProtectKernelDefaults:false NodeAllocatableConfig:{KubeReservedCgroupName: SystemReservedCgroupName: EnforceNodeAllocatable:map[pods:{}] KubeReserved:map[] SystemReserved:map[] HardEvictionThresholds:[{Signal:nodefs.inodesFree Operator:LessThan Value:{Quantity:&lt;nil&gt; Percentage:0.05} GracePeriod:0s MinReclaim:&lt;nil&gt;} {Signal:imagefs.available Operator:LessThan Value:{Quantity:&lt;nil&gt; Percentage:0.15} GracePeriod:0s MinReclaim:&lt;nil&gt;} {Signal:memory.available Operator:LessThan Value:{Quantity:100Mi Percentage:0} GracePeriod:0s MinReclaim:&lt;nil&gt;} {Signal:nodefs.available Operator:LessThan Value:{Quantity:&lt;nil&gt; Percentage:0.1} GracePeriod:0s MinReclaim:&lt;nil&gt;}]} ExperimentalQOSReserved:map[] ExperimentalCPUManagerPolicy:none ExperimentalCPUManagerReconcilePeriod:10s ExperimentalPodPidsLimit:-1 EnforceCPULimits:true}
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.023891    1982 container_manager_linux.go:266] Creating device plugin manager: true
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.024017    1982 state_mem.go:36] [cpumanager] initializing new in-memory state store
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.024170    1982 state_mem.go:84] [cpumanager] updated default cpuset: &quot;&quot;
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.024276    1982 state_mem.go:92] [cpumanager] updated cpuset assignments: &quot;map[]&quot;
jun 09 10:58:23 k8s-node2 kubelet[1982]: W0609 10:58:23.025777    1982 kubelet_network.go:139] Hairpin mode set to &quot;promiscuous-bridge&quot; but kubenet is not enabled, falling back to &quot;hairpin-veth&quot;
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.025914    1982 kubelet.go:558] Hairpin mode set to &quot;hairpin-veth&quot;
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.026729    1982 client.go:75] Connecting to docker on unix:///var/run/docker.sock
jun 09 10:58:23 k8s-node2 kubelet[1982]: I0609 10:58:23.026864    1982 client.go:104] Start docker client with request timeout=2m0s
jun 09 10:58:23 k8s-node2 kubelet[1982]: E0609 10:58:23.027044    1982 kube_docker_client.go:91] failed to retrieve docker version: Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
jun 09 10:58:23 k8s-node2 kubelet[1982]: W0609 10:58:23.027171    1982 kube_docker_client.go:92] Using empty version for docker client, this may sometimes cause compatibility issue.
jun 09 10:58:23 k8s-node2 kubelet[1982]: F0609 10:58:23.027409    1982 server.go:233] failed to run Kubelet: failed to create kubelet: failed to get docker version: Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
jun 09 10:58:23 k8s-node2 systemd[1]: <b>kubelet.service: Main process exited, code=exited, status=255/n/a</b>
jun 09 10:58:23 k8s-node2 systemd[1]: <b>kubelet.service: Unit entered failed state.</b>
jun 09 10:58:23 k8s-node2 systemd[1]: <b>kubelet.service: Failed with result &apos;exit-code&apos;.</b>
</pre>

Kubelet no encuentra una versión de docker ni tampoco el socket de este, por eso tengo que instalarlo. Con la versión que nos proporciona ubuntu en sus repositorios está bien:

```
apt install docker.io
```

Con esto ya puedo desplegar el clúster de Kubernetes con la red para flannel e ignorando la comprobación de requisitos ya que me da un error al comprobar el estado de Docker:

``` 
kubeadm init --pod-network-cidr=10.244.0.0/16 --ignore-preflight-errors=all
```

Una vez arrancado creo el fichero .kube/config para poder interactuar con el clúster:

```
cd && mkdir .kube && cp /etc/kubernetes/admin.conf $HOME/.kube/config
```

Y con el token que nos proporcio añado los nodos al clúster.

Una vez añadidos despliego la red de overlay, la que he elegido es flannel:

```
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.10.0/Documentation/kube-flannel.yml
```

Y con esto ya tenemos el clúster de Kubernetes levantado y funcionando ahora voy a hacer algunas comprobaciones:

#### Pods de sistema:

<pre>root@ubuntu:~# kubectl get pods -n kube-system
NAME                                    READY     STATUS    RESTARTS   AGE
etcd-ubuntu                             1/1       Running   10         13d
kube-apiserver-ubuntu                   1/1       Running   10         13d
kube-controller-manager-ubuntu          1/1       Running   10         13d
kube-dns-86f4d74b45-x5jst               3/3       Running   0          1h
kube-flannel-ds-b5zc7                   1/1       Running   7          13d
kube-flannel-ds-lbdvw                   1/1       Running   9          13d
kube-flannel-ds-q5885                   1/1       Running   0          53s
kube-proxy-dddxt                        1/1       Running   10         13d
kube-proxy-kszgn                        1/1       Running   0          53s
kube-proxy-ns4cg                        1/1       Running   7          13d
kube-scheduler-ubuntu                   1/1       Running   10         13d
kubernetes-dashboard-7d5dcdb6d9-gcv95   1/1       Running   0          1h
</pre>

Se ve como tengo todos los pods que necesito, un kube-proxy y kube-flannel por nodo, kube-dns, el apiserver, etcd, scheduler y el dashboard de kubernetes. Por aquí, todo correcto.

#### Runtime utilizado:

Voy a asegurar que todos los nodos estén utilizando cri-o como backend y no docker:

<pre>root@ubuntu:~# kubectl describe node ubuntu | grep Runtime
 Container <font color="#EF2929"><b>Runtime</b></font> Version:  cri-o://1.11.0-dev
root@ubuntu:~# kubectl describe node k8s-node1 | grep Runtime
 Container <font color="#EF2929"><b>Runtime</b></font> Version:  cri-o://1.11.0-dev
root@ubuntu:~# kubectl describe node k8s-node2 | grep Runtime
 Container <font color="#EF2929"><b>Runtime</b></font> Version:  cri-o://1.11.0-dev
</pre>

Y aunque Kubernetes diga que está utilizando cri-o, voy a comprobar en cada nodo con ```crictl``` los contenedores que están corriendo:

<pre>root@ubuntu:~# crictl ps
CONTAINER ID        IMAGE                                                                                                           CREATED             STATE               NAME                      ATTEMPT
4b3df50462894       k8s.gcr.io/kubernetes-dashboard-amd64@sha256:dc4026c1b595435ef5527ca598e1e9c4343076926d7d62b365c44831395adbd0   2 hours ago         Running             kubernetes-dashboard      0
91d4a37001ffe       f0fad859c909baef1b038ef8d2f6e76fc252e25a3d9af37b82ce70623fb7cd6f                                                2 hours ago         Running             kube-flannel              9
5d242fe8e6405       4261d315109d99f7149dc8de048ec12c11a77c3cd7d3479654f630c0c14c0c60                                                2 hours ago         Running             kube-proxy                10
d360da376ef7b       353b8f1d102e959e7865da53e9a63554789212f881e8d92d3289b3948c59c9be                                                2 hours ago         Running             kube-scheduler            10
c2bdc36f7e644       e03746fe22c35cb7434d36ac9cf9fd8bd4810af28081058e40db59e1cbe4a674                                                2 hours ago         Running             kube-apiserver            10
2c7881d78feaa       40c8d10b2d11cbc3db2e373a5ffce60dd22dbbf6236567f28ac6abb7efbfc8a9                                                2 hours ago         Running             kube-controller-manager   10
ee4e16b1fd6f5       52920ad46f5bf730d0e35e11215ec12d04ca5f32835f2ceb8093d0bd38930735                                                2 hours ago         Running             etcd                      10
root@ubuntu:~# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
</pre>

<pre>root@k8s-node1:~# crictl ps
CONTAINER ID        IMAGE                                                                                                              CREATED             STATE               NAME                ATTEMPT
ff86dba968f7e       18ea23a675dae45702b278215a4b6d20583a907a98b2b82d2af709c3ba86c76a                                                   2 hours ago         Running             nginx               0
23326d801a1f0       18ea23a675dae45702b278215a4b6d20583a907a98b2b82d2af709c3ba86c76a                                                   2 hours ago         Running             nginx               0
7e4f7d8fe9b7e       k8s.gcr.io/k8s-dns-sidecar-amd64@sha256:23df717980b4aa08d2da6c4cfa327f1b730d92ec9cf740959d2d5911830d82fb           2 hours ago         Running             sidecar             0
5fb60b3821a12       k8s.gcr.io/k8s-dns-dnsmasq-nanny-amd64@sha256:93c827f018cf3322f1ff2aa80324a0306048b0a69bc274e423071fb0d2d29d8b     2 hours ago         Running             dnsmasq             0
bc4de70f8bf6f       k8s.gcr.io/k8s-dns-kube-dns-amd64@sha256:6d8e0da4fb46e9ea2034a3f4cab0e095618a2ead78720c12e791342738e5f85d          2 hours ago         Running             kubedns             0
6036c71c2b2ab       docker.io/library/owncloud@sha256:abb9b13f96fcd70d62984d29a3bfe28c4aabedda91296c38a14d6ef1a0fabca5                 2 hours ago         Running             guacamole           2
51597b88025c2       18ea23a675dae45702b278215a4b6d20583a907a98b2b82d2af709c3ba86c76a                                                   2 hours ago         Running             nginx               2
1b52905bf8bf6       quay.io/external_storage/rbd-provisioner@sha256:d20241503a59414791608e0b5b7c6ceeb7212510723accecb292c8ce95fe570a   2 hours ago         Running             rbd-provisioner     2
99ee664814cb1       docker.io/oznu/guacamole@sha256:eb7809bce91260eb47414c8e526d846502cdef1344178164ace110798b71821e                   2 hours ago         Running             guacamole           5
48fcf060920ed       f0fad859c909baef1b038ef8d2f6e76fc252e25a3d9af37b82ce70623fb7cd6f                                                   2 hours ago         Running             kube-flannel        7
621ab279c673d       4261d315109d99f7149dc8de048ec12c11a77c3cd7d3479654f630c0c14c0c60                                                   2 hours ago         Running             kube-proxy          7
</pre>

<pre>root@k8s-node2:/home/ubuntu# crictl ps
CONTAINER ID        IMAGE                                                              CREATED             STATE               NAME                ATTEMPT
06d69e717edfb       f0fad859c909baef1b038ef8d2f6e76fc252e25a3d9af37b82ce70623fb7cd6f   10 minutes ago      Running             kube-flannel        0
e54d0ab7619eb       4261d315109d99f7149dc8de048ec12c11a77c3cd7d3479654f630c0c14c0c60   11 minutes ago      Running             kube-proxy          0
</pre>

### Ceph

Una vez tengo instalado el clúster de Kubernetes ya funcionando con cri-o, me queda montar el sistema de almacenamiento. Esto lo haré con ceph-deploy que lo obtendré desde los repositorios.

#### Preinstalación:

Importo la clave y repositorio e instalo los paquetes básicos:

```
sudo wget -q -O- 'https://ceph.com/git/?p=ceph.git;a=blob_plain;f=keys/release.asc' | sudo apt-key add -
sudo echo deb https://download.ceph.com/debian-luminous/ $(lsb_release -sc) main | sudo tee /etc/apt/sources.list.d/ceph.list
```

Instalación de paquetes en ceph-deployer:

```
sudo apt update && sudo apt install apt-transport-https ceph-deploy -y
```

En ceph1, ceph2, ceph3:

```
sudo apt update && sudo apt install ca-certificates apt-transport-https -y
```

Debo de añadir todos los nodos al /etc/hosts de ceph-deployer:

```
192.168.122.2	deployer
192.168.122.3	ceph1
192.168.122.4	ceph2
192.168.122.5	ceph3
```

Creo un usuario para ceph en todos los nodos y un par de claves ssh sin frase de paso en ceph-deployer:

```
sudo useradd -m -s /bin/bash deployer
```

Lo añado al grupo de sudoers:

```
echo "deployer ALL = (root) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/deployer && sudo chmod 0440 /etc/sudoers.d/deployer
```

Paso la clave a ceph1, ceph2 y ceph3:

```
ssh-copy-id deployer@ceph1 && ssh-copy-id deployer@ceph2 && ssh-copy-id deployer@ceph3
```

Configuro ssh:

``` 
nano .ssh/config

```

```
Host ceph1
 Hostname ceph1
 User deployer
Host ceph2
 Hostname ceph2
 User deployer
Host ceph3
 Hostname ceph3
 User deployer
```

#### Despliegue:

Lo primero que hice es logarme con el usuario que he creado y crear un directorio para el clúster:

```
su - deployer
mkdir cluster && cd cluster
```

Creo un nuevo despliegue y genero todos los tokens necesarios:

```
ceph-deploy new ceph-deployer
```

Y despliego ceph en todos los nodos:

```
ceph-deploy install ceph1 ceph2 ceph3 ceph-deployer
```

Ahora voy a crear el primer monitor y propagar los keyrings a todos los nodos del clúster:

~~~
ceph-deploy mon create-initial
ceph-deploy admin ceph-deployer ceph1 ceph2 ceph3
~~~

Con esto, toda la paquetería de ceph estará presente en los nodos de nuestro clúster. 

El siguiente paso es desplegar los OSD's:

~~~
ceph-deploy osd create ceph1:sdb ceph2:sdb ceph3:sdb
~~~

Una vez haya acabado el proceso, voy a comprobar el estado del clúster con ``` ceph -s ``` y nos devuelve HEALTH_OK, entonces prosigo.

Voy a crear el servidor de metadatos y el monitor:

```
ceph-deploy mds create ceph-deployer
ceph-deploy mon create ceph-deployer
```

Podría añadir un segundo monitor hay que crear un ``` quorum ``` pero sería en una máquina que no tuviera OSD.

Y en otro máquina añado el manager:

```
ceph-deploy mgr create ceph1
```

Solo falta RGW:

```
ceph-deploy rgw create ceph-deployer
```

Listo los pools disponibles:

<pre>root@deployer:~# ceph osd lspools
0 rbd,1 .rgw.root,2 default.rgw.control,3 default.rgw.data.root,4 default.rgw.gc,5 default.rgw.log,6 default.rgw.users.uid,7 k8s-pool,8 kube,
</pre>

### Despliegue de RBD-provisioner

RBD-provisioner es la implementación que permite crear volúmenes dinámicamente desde un clúster de Kubernetes, para será necesario una serie de pasos:

Desde el clúster de ceph creamos el secret de admin:

~~~
ceph auth get client.admin 2>&1 |grep "key = " |awk '{print  $3'} |xargs echo -n > /tmp/secret
kubectl create secret generic ceph-admin-secret --from-file=/tmp/secret --namespace=kube-system
~~~

Y además el pool y el secret del usuario:

~~~
ceph osd pool create kube 8 8
ceph auth add client.kube mon 'allow r' osd 'allow rwx pool=kube'
ceph auth get client.kube 2>&1 |grep "key = " |awk '{print  $3'} |xargs echo -n > /tmp/secret
kubectl create secret generic ceph-secret --from-file=/tmp/secret --namespace=kube-system
~~~

Estos secrets que se muestran por pantalla los he introducido en ```rbd-provisioner/secrets.yaml```, con esto ya puedo proceder a crear el aprovisonamiento dinámico:

~~~
kubectl create -f rbd-provisioner/
~~~

rbd-provisioner se despliega en el namespace default pero accede a los secrets que están en el namespace kube-system, esto es la principal dificultad que me encontré en su despliegue. 
En la documentación de github, en clusterrole.yaml observamos los permisos que le asignan y faltan los permisos de "get" y "list" secrets en dicho namespace, por ello los añadí:

~~~
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: rbd-provisioner
rules:
  - apiGroups: [""]
    resources: ["persistentvolumes"]
    verbs: ["get", "list", "watch", "create", "delete"]
  - apiGroups: [""]
    resources: ["persistentvolumeclaims"]
    verbs: ["get", "list", "watch", "update"]
  - apiGroups: ["storage.k8s.io"]
    resources: ["storageclasses"]
    verbs: ["get", "list", "watch"]
  - apiGroups: [""]
    resources: ["events"]
    verbs: ["list", "watch", "create", "update", "patch"]
  - apiGroups: [""]
    resources: ["services"]
    resourceNames: ["kube-dns"]
    verbs: ["list", "get"]
  - apiGroups: [""]
    resources: ["secrets"]
    verbs: ["get", "list"]
~~~

El estado que tiene que quedar es el siguiente:

<pre>root@ubuntu:~# kubectl get pods 
NAME                              READY     STATUS    RESTARTS   AGE
rbd-provisioner-bc956f5b4-ft7r2   1/1       Running   3          7d
</pre>

## Funcionamiento

Para comprobar el funcionamiento del clúster voy a desplegar una aplicación que haga uso de ese volumen, utilizaré owncloud. Pero antes voy a explicar como funciona rbd-provisioner:

![rbd-provisioner](/images/rbd-provider.png)

1. Insertamos yaml solicitando recursos.
2. Apiserver empieza a crear pods, deployments, daemonsets...
3. La api busca el endpoint del storageclass que está en el pod rbd-provisioner
4. Rbd-provisioner con los secrets y permisos sobre el pool solicita el volumen
5. Rbd-provisioner le pasa el volumen a la api
6. La api crea el pv y el pvc.

Despliegue de owncloud: 

![owncloud-deploy.yaml](/deploy/owncloud-deploy.yaml)

## Conclusión

La idea principal que he sacado de este proyecto es que una solución basada en Ceph ha de estar separada del gestor de contenedores ya que al intentar desplegar 
rook.io para la integración con ceph a los días de estar desplegado con aplicaciones los pods entraban en estado de ```error``` y se perdía la información. Mientras que con RBD-provisioner y un clúster externo ha sido totalmente consistente.

El resultado de la unión de estas dos tecnologías tan elásticas creo que es muy interesante ya que podemos incrementar la capacidad de ambas tecnologías tanto como hardware tengamos o incluso crecer en la nube
teniendo entornos híbridos.

Cri-o aparentemente parecía que iba a ser un backend para Kubernetes más ligero que Docker por la ausencia del entorno y por estar expresamente enfocadoa Kubernetes, pero para mi sorpresa es que de momento no, es igual que Docker
y en algunos momentos incluso más pesado. Esto posiblemente puede deberse al poco tiempo de Cri-o y la falta de optimización.

## Trabajo por hacer

El trabajo que queda por hacer es todavía amplio, puedo decir que este el primer paso para tener un cluster de Kubernetes listo para producción con altas cargas de trabajo, si en el futuro sigo con el proyecto o alguien quiere seguirlo dejo aquí una lista de tareas pendientes que bajo mi opinión serían interesantes:

* Alta disponibilidad del nodo master.
* Recolección de métricas con el operador de prometheus.
* Centralización de logs con fluentd
* Instalación de KubeVirt para alta disponibilidad de los nodos.
* Integración de multiarquitectura por si se integraran nodos ARM.

## Fuentes

Las fuentes han sido sobretodo documentaciones oficiales de las herramientas utilizadas.

Para Ceph:

https://manuelfrancoblog.wordpress.com/2018/02/19/ceph/

http://apprize.info/web/ceph/1.html

http://docs.ceph.com/docs/hammer/architecture/

https://www.ull.es/servicios/stic/2016/11/11/sistema-de-almacenamiento-ceph/

Cri-o:

https://github.com/kubernetes-incubator/cri-o/blob/master/tutorial.md

https://github.com/kubernetes-incubator/cri-o/blob/master/kubernetes.md

Kubernetes:

https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/

Rbd-provisioner:

https://github.com/kubernetes-incubator/external-storage/tree/master/ceph/rbd
